#' @title Function to group measures in samples by Celltype
#' @description  This function allows you to group the measures present in the batch dataframes
#' loaded by the \code{importbatchtables} function, see \code{?importbatchtables}
#' by celltypes defined in an indextable (see below)
#' 
#' @param indextable small table with a first column of samples (A01, etc) and one or more columns of celltypes measures
#' @param batchtables set of dataframes imported with the \code{importbatchtables} function
#' @keywords filter 
#' @export
#' @examples
#' 
#' #Creating a small indextable
#' indextable<-read.table(text="
#' sample  celltype1 celltype2
#' A01     M4        P3
#' A02     M10
#' A03     NA        M5" ,  header=TRUE, stringsAsFactors=FALSE,fill=TRUE, na.strings=c("","NA"))
#' indextable
#' 
#' Note: this function will filter and group your batch tables based on the 
#' columns, which represent celltypes in the columns detailed in the indextable.
#' You can use any names for the columns
#' 
#' #Creating the list of dataframes of the batch tab (previous step)
#' #with the function \code{importbatchtables}
#' filename<-file.choose() # an excel file with all the tables from the Batch tab of the accuri software.
#' batchtableslist<-importbatchtables(filename)
#' 
#' #Getting the info for your celltypes from the batchtable based on the small indextable
#' matchBatchtablesbyCelltype(indextable,batchtableslist)
#' you will get a table(s) with the measures of the celltypes of interest
#' if you select to see results as one sheet, you will have summary statistics also.
matchBatchtablesbyCelltype<-function(indextable,batchtables,response,newfilename,addmeans){ 
  ncols<-ncol(indextable)
  celltypes<-NULL
  celltypes<-list()
  for(i5 in 2:ncols) {
    namecelltype<-names(indextable)[i5]
    indextabfilter<-indextable[,c(1,i5)]
    indextabfilter<- indextabfilter[complete.cases(indextabfilter),]
    nrowsn<-nrow(indextabfilter)
    for(i3 in 1:nrowsn) {
      fil<-indextabfilter[i3,]
      selecteddf<-grep(paste0("(?=.*",fil[1,][1],")(?=.*",fil[1,][2],")"), names(batchtables), perl = TRUE)
      lenselec<-length(selecteddf)
      if(length(selecteddf)==0){
       print(paste(length(selecteddf),"results found for",fil[1,][1],fil[1,][2]) )  }
      else {
          list1<-list()
          list1 <-batchtables[selecteddf]
          for (i4 in 1:lenselec) {
          list1[[i4]]<-list1[[i4]][grep(unlist(fil[1,][2]), list1[[i4]][,1]), ]
          if(nrow(list1[[i4]])==0){list1[[i4]]<-NA}#list(NULL)}
          colnames(list1[[i4]])[1]<-namecelltype
          } # end i4
          list1<-list1[!is.na(list1)]
          celltypes[[namecelltype]]<-c(celltypes[[namecelltype]], list1)
          celltypes[[namecelltype]]<-unique(celltypes[[namecelltype]])
          celltypes[[namecelltype]]<-celltypes[[namecelltype]][!sapply(celltypes[[namecelltype]], is.null)] 
          names(celltypes[[namecelltype]]) <- sapply(celltypes[[namecelltype]], attr, 'name')
          names(celltypes[[namecelltype]])<-gsub(":|/","",names(celltypes[[namecelltype]]))
          names(celltypes[[namecelltype]])<-strtrim(names(celltypes[[namecelltype]]),27)
          print(paste(length(selecteddf),"results found for",fil[1,][1],fil[1,][2]) )
      } # end else
    } #end for i3
  } #for i5
  #batchtables<-batchtableslist
  celltypes<-lapply(celltypes, function (listofdf)  {  
    lapply(listofdf, function(df) {
    df[ , ! apply( df , 2 , function(x) all(is.na(x)) ) ]
    } )
  } )
  celltypes<-lapply(celltypes, function (listofdf)  {  
     rbind.fill(listofdf)
  })
  celltypes<-lapply(celltypes, function (df)  {  
      unique(df)} )
  celltypes<-lapply(celltypes, function (df) {
                     colnames(df)<-gsub(("[\u03BC|µ]"),"u",colnames(df) )
                     return(df)}
  )
  readfilename <- function(){
    n <- as.character(readline(prompt="Please, enter name of file: ") )
  }
  repeat {
    text<-"Save all celltypes in a single sheet (1), or separated in sheets (2); Write 1 or 2: "
    text<-paste(strwrap(text,60), " ",collapse="\n")
    if(missing(response)){
      #response <- as.numeric(readline(paste("Save all tables in a single sheet (1), or separeted in sheets (2), Write a number!:")))
      response <- as.numeric(readline(paste(text)))
      }
      if (response == 2) {
      
      print("You chose several sheets, they will be saved as .xls file and as 'celltypes' list object")
      #celltypes<<-celltypes
      if(missing(newfilename)){
        newfilename<-readfilename()
        #response <- as.numeric(readline(paste("Save all tables in a single sheet (1), or separeted in sheets (2), Write a number!:")))
      }
      if(missing(addmeans)){
        addmeans<-menu(c("Yes", "No"), title="Do you want to add sums and means to tables?")
      }
      if (addmeans==1)
      {
        #colnames(celltypes)<-gsub(("[\u03BC|µ]"),"u",colnames(celltypes))
        #celltypes<-makenumcols(celltypes)  # function
        #celltypes<-cellty
        celltypes<-lapply(celltypes, function(df) makenumcols(df) )  # function
        celltypes<-lapply(celltypes, function (df) setDT(df) )
        celltypes<-lapply(celltypes, function (df)
        {
        df<-cbind(celltype=paste(colnames(df)[1]),df)
        onesum<-df[, lapply(.SD, sum, na.rm=TRUE), .SDcols=colnames(df)[c(3:length(colnames(df))) ] ]
        onesum$celltype<-paste0(onesum$celltype,"sums")
        onemean<-df[, lapply(.SD, mean, na.rm=TRUE), .SDcols=colnames(df)[c(3:length(colnames(df))) ] ]
        onemean$celltype<-paste0(onemean$celltype,"means")
        df<-rbindlist(list(df,onesum,onemean), fill=T)
        return(df)
        }
       # ?rbindlist
        )
        celltypes<<-celltypes
      } # end addmeans True
      celltypes<<-celltypes
      #  celltypes[1]
      #newfilename<-readfilename()
      tryCatch( {
      #WriteXLS(celltypes,paste0(newfilename,".xls") )
      WriteXLS(celltypes,paste0(newfilename,".xls") )
      #6+2
      print(paste("file created:",paste0(getwd(),"/",newfilename,".xls"),
                  "and object celltypes" ) )
      },  
    warning=function(w) {
      print(paste("Error writing .xls, go to https://github.com/marcschwartz/WriteXLS/blob/master/INSTALL to download perl for Windows, set PATH with SET PATH=%PATH%;C:/perl64/bin or similar, then reboot RStudio"
      )) },
    error=function(e) {
      print(paste("Error writing .xls, go to https://github.com/marcschwartz/WriteXLS/blob/master/INSTALL to download perl for Windows, set PATH with SET PATH=%PATH%;C:/perl64/bin or similar, then reboot RStudio"
      )) }
    ) # end tryCatch
      
     # print(paste("file created:",paste0(getwd(),"/",newfilename,".xls"),"and object celltypes" ) )
      invisible(celltypes)
      break
    } # end response == 2
    else if (response == 1) {
      print("You chose 1 sheet, it will be saved in a .xls file and as 'celltypes' df object!")
      for (i6 in 1:length(celltypes)){
        celltypes[[i6]]<-cbind(celltype=paste(colnames(celltypes[[i6]])[1]),celltypes[[i6]])
        celltypes[[i6]][,2]<-paste(colnames(celltypes[[i6]])[2],celltypes[[i6]][,2])
        colnames(celltypes[[i6]])[2]<-"celltype_and_regions"
      }
      celltypesbac<-celltypes
      celltypes<- rbind.fill(celltypes)
      if(missing(addmeans)){
      addmeans<-menu(c("Yes", "No"), title="Do you want to add sums and means to table?")
      }
      if (addmeans==1)
      {
      #colnames(celltypes)<-gsub(("[\u03BC|µ]"),"u",colnames(celltypes))
      celltypes<-makenumcols(celltypes)  # function
      celltypes<-setDT(celltypes)
      onesum<-celltypes[, lapply(.SD, sum, na.rm=TRUE), by=celltype, .SDcols=colnames(celltypes)[c(3:length(colnames(celltypes))) ] ] 
      
      onesum$celltype<-paste0(onesum$celltype,"_sums")
      onemean<-celltypes[, lapply(.SD, mean, na.rm=TRUE), by=celltype, .SDcols=colnames(celltypes)[c(3:length(colnames(celltypes))) ] ] 
      onemean$celltype<-paste0(onemean$celltype,"_means")
      celltypes<-c(celltypesbac,list(as.data.frame(onesum)),list(as.data.frame(onemean)))#,onemean)
      
      celltypes<- rbind.fill(celltypes)
      } # end add means true
      celltypes<-makenumcols(celltypes)
      #colnames(celltypes)<-gsub(("[\u03BC|µ]"),"u",colnames(celltypes))
      celltypes<<-celltypes
      if(missing(newfilename)){
        newfilename<-readfilename()
        #response <- as.numeric(readline(paste("Save all tables in a single sheet (1), or separeted in sheets (2), Write a number!:")))
      }
      #newfilename<-readfilename()
      #newfilename<-"lj"
      tryCatch({
        WriteXLS(celltypes,paste0(newfilename,".xls") )
        #6+2
        print(paste("file created:",paste0(getwd(),"/",newfilename,".xls"),
                    "and object celltypes df" ) )
        },  
        warning=function(w) {
          print(paste("Error writing .xls, go to https://github.com/marcschwartz/WriteXLS/blob/master/INSTALL to download perl for Windows, set PATH with SET PATH=%PATH%;C:/perl64/bin or similar, then reboot RStudio"
          )) },
        error=function(e) {
          print(paste("Error writing .xls, go to https://github.com/marcschwartz/WriteXLS/blob/master/INSTALL to download perl for Windows, set PATH with SET PATH=%PATH%;C:/perl64/bin or similar, then reboot RStudio"
          )) }
    
        )
      
      invisible(celltypes)
      break
    } # end all in one sheet
    else {
      print("Sorry, the answer the question MUST be 1 or 2, otherwise press [Esc]")
    }
  } # end repeat
  invisible(celltypes)
} # end function
