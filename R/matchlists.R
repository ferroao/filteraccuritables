#' Function to Match Lists
#'
#' This function allows you to compare two lists
#' @param yourlist the first list of elements to be compared with "tolist"
#' @param tolist the second list of elements to be compared with yourlist
#' @keywords lists
#' @export
#' @examples
#' matchlists(c(1,2,3),c(2,4,5))
#' #[1] "2"
matchlists <- function (yourlist, tolist){
  elementsmatched <- unique (grep(paste(tolist,collapse="|"), yourlist, value=TRUE))
  return(elementsmatched)
}

  

