#' Function to select some tables from all the imported Batch tables
#' @description 
#' This function allows you to select some dataframes (tables)
#' from the list of dataframes that have been loaded with the
#' \code{importbatchtables} function, see \code{?importbatchtables}
#' 
#' @description 
#' Note: this function will filter your batch tables based on the information
#' in the rows of the indextable; but, in order to group the information by 
#' columns (e.g. M4, M10) you need to use the function \code{matchBatchtablesbyCelltype}

#' 
#' @param indextable small table with a first column of samples (A01, etc) and one or more columns of regions or ranges
#' @param batchtables set of dataframes imported with the \code{importbatchtables} function
#' @keywords filter 
#' @export
#' @examples
#' #Creating a small indextable. You can use any names for 
#' #the columns
#' indextable<-read.table(text="
#' sample  r1     r2
#' A01     M4     P3
#' A02     M10" ,  header=TRUE, stringsAsFactors=FALSE,
#' fill=TRUE, na.strings=c("","NA"))
#' 
#' #Creating the list of dataframes of the batch tab 
#' #(previous step)
#' #with the function importbatchtables
#' filename<-file.choose() # an excel file with all the
#' # tables from the Batch tab of the accuri software.
#' batchtableslist<-importbatchtables(filename)
#' 
#' #Matching indextable with the batchtable
#' filterBatchTablesbyIndex(indextable,batchtableslist)
#' #you will get a subset of tables with the samples and 
#' #ranges of the indextable

filterBatchTablesbyIndex<-function(indextable,batchtables,response,newfilename){ 
  nrowsn<-nrow(indextable)
  listoftables<-NULL
  listoftables<-list()
  list1<-NULL
  list1<-list()
  counter<-0
  for(i in 1:nrowsn) {
    res<-t(combn(Filter(function(x)!all(is.na(x)), indextable[i,] ),2))
    fil<-res[res[,1] == indextable[i,1],,drop=F]
    filn<-nrow(fil)
    for (i3 in 1:filn){
      selecteddf<-grep(paste0("(?=.*",fil[i3,][1],")(?=.*",fil[i3,][2],")"), names(batchtables), perl = TRUE)
      if(length(selecteddf)==0){
        print(paste(length(selecteddf),"results found for",fil[i3,][1],fil[i3,][2]) )  }
      else {
        list1 <-batchtables[selecteddf]
        listoftables<-c(listoftables,list1)
        print(paste(length(selecteddf),"results found for",fil[i3,][1],fil[i3,][2]) )
        counter<-counter+1
      } # end else
    } #end for
  } # end for
  listoftables<-unique(listoftables)
  listoftables<-listoftables[!sapply(listoftables, is.null)] 
  names(listoftables) <- sapply(listoftables, attr, 'name')
  names(listoftables)<-gsub(":|/","",names(listoftables))
  names(listoftables)<-strtrim(names(listoftables),27)
  readfilename <- function(){
    n <- as.character(readline(prompt="Please, enter name of file: ") )
  }
  if (counter==0){return()} else {
    listoftables<-lapply(listoftables, function (df) {
      colnames(df)<-gsub(("[\u03BC|µ]"),"u",colnames(df) )
      return(df)}
    )
  repeat {
    if(missing(response)){
    response <- as.numeric(readline(paste("Save all tables in a single sheet (1), or separeted in sheets (2), Write a number!: ")))
    }
    if (response == 2) {
      print("You chose several sheets, they will be saved as .xls file and as 'listoftables' object")
      listoftables<<-listoftables
      if(missing(newfilename)){
        newfilename<-readfilename()
        #response <- as.numeric(readline(paste("Save all tables in a single sheet (1), or separeted in sheets (2), Write a number!:")))
      }
      #newfilename<-readfilename()
#      WriteXLS(listoftables,paste0(newfilename,".xls") )
      
      tryCatch( {
        WriteXLS(listoftables,paste0(newfilename,".xls") )
        print(paste("file created:",paste0(getwd(),"/",newfilename,".xls")) )
      },  
      warning=function(w) {
        print(paste("Error writing .xls, go to https://github.com/marcschwartz/WriteXLS/blob/master/INSTALL to download perl for Windows, set PATH with SET PATH=%PATH%;C:/perl64/bin or similar, then reboot RStudio"
        )) },
      error=function(e) {
        print(paste("Error writing .xls, go to https://github.com/marcschwartz/WriteXLS/blob/master/INSTALL to download perl for Windows, set PATH with SET PATH=%PATH%;C:/perl64/bin or similar, then reboot RStudio"
        ) ) }
      ) # end tryCatch
      break
    }
    else if (response == 1) {
      print("You chose 1 sheet, it will be saved in a .xls file and as 'alltables1df' object!")
      listoftables<-lapply(listoftables,function(x) rbind(colnames(x), x) ) #repite name cols
      listoftables<-lapply(listoftables, function(x) {colnames(x)<-1:length(colnames(x))
      return(x) # da nombres de columnas de números
      })
      alltables1df <<- Reduce(rbind, listoftables) #SOLO 1 DF
      if(missing(newfilename)){
        newfilename<-readfilename()
        #response <- as.numeric(readline(paste("Save all tables in a single sheet (1), or separeted in sheets (2), Write a number!:")))
      }
      #newfilename<-readfilename()

      tryCatch( {
        WriteXLS(alltables1df,paste0(newfilename,".xls") ) 
        # print(paste("file created:",paste0(getwd(),"/",newfilename,".xls"),
        #             "and object celltypes" ) )
        print(paste("file created:",paste0(getwd(),"/",newfilename,".xls")) )
              },  
      warning=function(w) {
        print(paste("Error writing .xls, go to https://github.com/marcschwartz/WriteXLS/blob/master/INSTALL to download perl for Windows, set PATH with SET PATH=%PATH%;C:/perl64/bin or similar, then reboot RStudio"
        )) },
      error=function(e) {
        print(paste("Error writing .xls, go to https://github.com/marcschwartz/WriteXLS/blob/master/INSTALL to download perl for Windows, set PATH with SET PATH=%PATH%;C:/perl64/bin or similar, then reboot RStudio"
        ) ) }
       
      ) # end tryCatch

     break
    } else {
      print("Sorry, the answer the question MUST be 1 or 2")
    }
  } # end repeat
  }
} # end function
