#' Function to get the appropriate variables from a stat table based on an index table
#'
#' This function allows you to get the matches of your index table (A01 M1, etc)
#' that are present in the original statistic tab table
#' This funcion requires the functions \code{compare_samples, compare_ranges, matchlists} from the
#' same package
#'
#' @param indextab the object containing an index of ranges of interest per sample (A01, A02, etc.)
#' @param bigtab the name of imported table from the statistic tab using \code{readStatisticTabCsvOdsXls()}
#' @keywords match
#' @export
#' @examples
#' indextable<-read.table(text="
#' well celltype1 celltype2
#' A01  M36    M37
#' A02  M43    M44
#' A03  M48    M49" ,  header=TRUE, stringsAsFactors=FALSE,fill=TRUE, na.strings=c("","NA"))
#'
#' filteredtable<-matchIndextoStat(indextable,readStatisticTabCsvOdsXls())
#' matchIndextoStat(indextable,statistictabtable)
matchIndextoStat<-function(indextab,bigtab){
  bigtab<-as.data.frame(bigtab)
  indextab<-as.data.frame(indextab)
  df<-indextab
  compare_samples(df,bigtab)
  listofbig<-unlist(bigtab[,1])
  listofsmall<-unlist(df[,1])
  additional<-listofsmall[!listofsmall%in%listofbig]
  if (length(additional)>0){
    df<-subset(df, (eval(parse(text=colnames(df)[1])))!=additional)
  }
  compare_ranges(df,bigtab)
  nrowsn<-nrow(df)
  ncolsn<-ncol(df)
  x <- vector("list", nrowsn)
  y<- vector("list", ncolsn)
  for(i in 1:nrowsn) {
    for(i2 in 2:ncolsn) {
      y[[1]]<-df[,1][i]
      Ps <- bigtab[bigtab[,1] %in% df[,1][i], matchlists(colnames(bigtab),c(df[,i2][i] ) ) ]#i  ## where i is whatever your Ps is
      colnames(Ps)[grep(df[,i2][i], colnames(Ps) )]<-paste0(colnames(df)[i2],sub(df[,i2][i],"",colnames(Ps)[grep(df[,i2][i], colnames(Ps) )]) )
      y[[i2]]<- Ps
    }
    result = Reduce(function(...) merge(..., all=T), y)
    x[[i]] <- result
  }
  result2 = Reduce(function(...) merge(..., all=T), x)
  colnames(result2)[1]<-"sample"
  #return(result2)
  indextoStatTable<<-as.data.frame(result2)
  print("the object indextoStatTable has been created")
  invisible(indextoStatTable)
  }
