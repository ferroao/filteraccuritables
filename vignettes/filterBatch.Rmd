---
title: "Filtrando tabelas da aba Batch"
author: "Fernando Roa"
date: "20 06 2017"
output: rmarkdown::html_vignette

vignette: >
  %\VignetteIndexEntry{Filtrando tabelas da aba Batch}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

Esta guia mostra como usar as funções relacionadas com o processamento de tabelas da aba Batch do software do citômetro accuri. Normalmente essa aba vai conter muitas tabelas com regiões (P1) e intervalos (M1) que não correspondem à amostra de interesse.
Por isso, esta guia permite filtrar a informação relevante.

## 0. Carregar o package

Se já instalou o package não precisa fazer o seguinte:

```
    # isto instala o package devtools, necessário para o seguinte passo
    install.packages("devtools")

    devtools::install_github("ferroao/filteraccuritables",  build_vignettes = TRUE, force=T) 
    # O anterior, instala o package filteraccuritables, por isso só é feito uma única vez
    
    browseVignettes("filteraccuritables") # abrir a página das ajudas (vignettes)
```
```{r, results="hide", message=FALSE, warning=FALSE}
    library(filteraccuritables) # carregar o package, uma vez por sessão no R ou RStudio
```

## 1. Salvando seus dados

Inicialmente você deve ir no software do accuri, e exportar para o Excel (.xls) as tabelas da aba Batch.

## 2. Carregando seus dados
Posteriormente, se você está no RStudio, utilize o menú, "Session","Set working directory" para escolher sua pasta de trabalho manualmente, ou utilize um comando como: 

```
setwd("~/Citometria/suapasta")
```
Agora começaremos a usar uma função do package, para abrir a tabela que você salvou.
```
importbatchtables(file.choose()) 
#isto cria o objeto (lista) "batchtableslist" com as tabelas, ou dê um nome
lista<-importbatchtables(file.choose())
``` 
Se deseja abrir um arquivo de exemplo em vez de seu arquivo, use:
```{r, echo=TRUE, warning=FALSE, results="hide"}
filename<-system.file("extdata", "exporttest.xls", package = "filteraccuritables")
importbatchtables(filename)
```
O objeto batchtableslist (de exemplo) tem 27 tabelas
A primeira delas:
```{r,echo=FALSE,results='asis'}
knitr::kable(batchtableslist[1])
```


##3. (Opcional) ver em formato de tabela os titulos das batchtables
```{r,echo=FALSE,results='asis'}
summary<-regionsInBatch(batchtableslist)
knitr::kable(head(summary,5)) 
```

```
# conteúdo das 5 primeiras tabelas das 27
```


##4. Criar tabela indice
Agora você tem que fazer uma pequena tabela contendo as regiões ou intervalos de cada amostra para 
fazer uma seleção das tabelas da aba Batch 
Um exemplo:
```{r}
indextable<-read.table(text="
well    r1     r2
A01     M4     P3
A02     M10" ,  header=TRUE, stringsAsFactors=FALSE,fill=TRUE, na.strings=c("","NA"))
# as colunas r1 e r2 podem ser tipos celulares por exemplo, ou espécies diferentes
```

##5. filtrar BatchTables pela Indextable. Obter apenas as tabelas de interesse
```
filterBatchTablesbyIndex(indextable,batchtableslist)
#você pode escolher se cada tabela fica numa página nova, ou todas juntas
#e o nome do arquivo resultante
#esta função não agrupa os resultados pelas colunas (r1 e r2), para isso use o passo 6.
``` 
Resultado de exemplo:

```{r,echo=FALSE}
filterBatchTablesbyIndex(indextable,batchtableslist,1,"nameoffile")
```

##6. Fazer resumo por tipo celular, 
```
matchBatchtablesbyCelltype(indextable,batchtableslist)
#você pode escolher se cada tipo celular (r1 e r2 no exemplo) fica numa página só, ou todos juntas,
#o nome do arquivo resultante e fazer somas ou médias
```
Resultado de exemplo:
```{r, echo=FALSE}
matchBatchtablesbyCelltype(indextable,batchtableslist,1,"new",1)
```

```{r,echo=FALSE,results='asis'}
knitr::kable(celltypes, format = "html")
```


